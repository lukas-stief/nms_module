// #include "C:/Python368/include/Python.h"
#include "C:/Program Files/Python37/include/Python.h"
#include <iostream>
#include <vector>
#include <stdexcept>

#include "BoxContainer.h"
#include "NonMaximumSuppression.h"

// PyObject* vectorToListLong(const std::vector<long> &data) {
//   PyObject* listObj = PyList_New( data.size() );

// 	if (!listObj) throw std::logic_error("Unable to allocate memory for Python list");
// 	for (unsigned int i = 0; i < data.size(); i++) {
// 		PyObject *num = PyLong_FromLong( (long) data[i]);
// 		if (!num) {
// 			Py_DECREF(listObj);
// 			throw std::logic_error("Unable to allocate memory for Python list");
// 		}
// 		PyList_SET_ITEM(listObj, i, num);
// 	}
// 	return listObj;
// }

// std::vector<long> listToVectorLong(PyObject *pyList)
// {
//     PyObject *pyListItem;
//     long listItem;

//     int pyListLen = PyList_Size(pyList);

//     std::vector<long> outputVector(pyListLen);
//     for (int i = 0; i < pyListLen; i++)
//     {
//         pyListItem = PyList_GetItem(pyList, i);
//         listItem = PyLong_AsLong(pyListItem);

//         outputVector[i] = listItem;
//     }

//     return outputVector;
// }

// static PyObject * addValueToEachListElement(PyObject *self, PyObject *args)
// {
//     PyObject *pyList;
//     PyObject *pyListItem;

//     long inputLong;

//     if (!PyArg_ParseTuple(args, "i|O", &inputLong, &pyList)) return NULL;

//     std::vector<long> listAsVector = listToVectorLong(pyList);
//     for (std::vector<long>::iterator it = listAsVector.begin(); it != listAsVector.end(); ++it)
//     {
//         *it += inputLong;
//     }

//     return vectorToListLong(listAsVector);
// }


// static PyObject * printEachElement(PyObject * self, PyObject *args)
// {
//     BoxContainer::sayHi();
//     PyObject *pyList;
//     PyObject *pyListElement;
//     PyObject *pyElementList;
//     PyObject *pyElementListElement;
//     long listItem;
//     std::vector<long> listAsVector;

//     PyArg_ParseTuple(args, "O", &pyList);
//     int pyListLength = PyList_Size(pyList);
//     int secondPyListLength = 0;
//     for (unsigned int i = 0; i < pyListLength; i++)
//     {
//         pyListElement = PyList_GetItem(pyList, i);
//         std::vector<long> listAsVector = listToVectorLong(pyListElement);
//         for (std::vector<long>::iterator it = listAsVector.begin(); it != listAsVector.end(); ++it)
//         {
//             std::cout << *it;
//         }
//         std::cout << "---" << "\n";
//     }

// }

// static PyObject * boxContainerWrapper(PyObject * self, PyObject *args)
// {
//     PyObject *pyList;
//     PyArg_ParseTuple(args, "O", &pyList);

//     BoxContainer boxContainer = BoxContainer::fromPyList(pyList);
//     return pyList;
// }

static PyObject * boxContainerWrapper(PyObject * self, PyObject *args)
{
    PyObject *pyList;
    PyObject *filteredPyList;
    float overlapThreshold;
    PyArg_ParseTuple(args, "O|f", &pyList, &overlapThreshold);

    BoxContainer boxContainer = BoxContainer::fromPyList(pyList);
    NonMaximumSuppression nms = NonMaximumSuppression(boxContainer);
    nms.suppress(overlapThreshold);
    BoxContainer filteredBoxContainer = nms.getBoxContainer();
    filteredPyList = filteredBoxContainer.toPyList();
    return filteredPyList;
}

static PyMethodDef SpamMethods[] = {
    // {"add_value_to_each_list_item", addValueToEachListElement, METH_VARARGS,
    //  "Add plus ten to each integer item of a list."},
    //  {"print_each_element", printEachElement, METH_VARARGS,
    //  "Add plus ten to each integer item of a list."},
    {"nms", boxContainerWrapper, METH_VARARGS,
    "Add plus ten to each integer item of a list."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


static struct PyModuleDef spammodule = {
    PyModuleDef_HEAD_INIT,
    "spam",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    SpamMethods
};


PyMODINIT_FUNC PyInit_spam(void)
{
    return PyModule_Create(&spammodule);
}


int main(int argc, char *argv[])
{
    wchar_t *program = Py_DecodeLocale(argv[0], NULL);
    if (program == NULL) {
        fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
        exit(1);
    }

    /* Add a built-in module, before Py_Initialize */
    PyImport_AppendInittab("spam", PyInit_spam);

    /* Pass argv[0] to the Python interpreter */
    Py_SetProgramName(program);

    /* Initialize the Python interpreter.  Required. */
    Py_Initialize();

    /* Optionally import the module; alternatively,
       import can be deferred until the embedded script
       imports it. */
    PyImport_ImportModule("spam");

    PyMem_RawFree(program);
    return 0;
}
