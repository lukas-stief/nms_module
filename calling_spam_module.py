import spam

# [center_x, center_y, width, height, rotation, loss]
example_list = [[10, 20, 10, 5, 0, 0.8],
                [10, 20, 10, 5, 90, 0.6],
                [12, 22, 10, 5, 90, 0.7],
                [10, 20, 10, 5, 90, 0.55],
                [10, 20, 10, 5, 90, 0.56],
                [10, 20, 10, 5, 90, 0.99], ]
# example_list = []

example_list = spam.nms(example_list, 0.5)

print(example_list)
