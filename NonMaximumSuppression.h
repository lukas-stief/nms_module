#ifndef _NONMAXIMUMSUPRESSION_H
#define _NONMAXIMUMSUPRESSION_H

#include "Box.h"
#include "BoxContainer.h"
#include "RectangleVertices.h"

class NonMaximumSuppression
{
private:
    BoxContainer _boxContainer;
public:
    NonMaximumSuppression(BoxContainer boxes);
    // ~NonMaximumSuppression();

    void suppress(float overlapThreshold);

    BoxContainer getBoxContainer();
};

#endif
