#include "BoxContainer.h"
#include "Box.h"

BoxContainer::BoxContainer()
{
    std::list<Box> emptyBoxesList;
    _boxes = emptyBoxesList;
}

BoxContainer::BoxContainer(std::list<Box> boxesListParameter)
{
    _boxes = boxesListParameter;
    sortByBoxesLossValuesDescending();
}

BoxContainer BoxContainer::fromPyList(PyObject *pyList)
{
    // TODO: Check weather the given list is of valid shape or not
    // if isValidBoxEntryList(pyList)
    PyObject *pyBoxListItem;
    PyObject *pyListBoxEntry;

    int amountOfBoxes = PyList_Size(pyList);
    std::list<Box> boxesList;

    for (unsigned int i = 0; i < amountOfBoxes; i++)
    {
        pyListBoxEntry = PyList_GetItem(pyList, i);

        // TODO: Check weather the given list entry is of valid shape or not
        // if isValidBoxEntry(pyListBoxEntry)
        Box box = BoxContainer::pyListToBox(pyListBoxEntry);
        boxesList.push_back(box);

    }
    // Box box1 = boxesList.front();
    // Box box2 = boxesList.back();

    // RectangleVertices rectVertBox1 = RectangleVertices::fromBox(box1);
    // RectangleVertices rectVertBox2 = RectangleVertices::fromBox(box2);
    // float area = rectVertBox1.intersectionAreaWith(rectVertBox2);
    // std::cout << "overlap area: " << area << "\n";

    return BoxContainer(boxesList);
}

Box BoxContainer::pyListToBox(PyObject *pyBoxList)
{
    PyObject *pyBoxListItem;
    int boxListLength = PyList_Size(pyBoxList);

    // TODO: cleaner initialization of Boxes instead of switch case
    int centerX, centerY, width, height, rotation = 0;
    double loss = 0;
    for (int i = 0; i < boxListLength; i++)
    {
        pyBoxListItem = PyList_GetItem(pyBoxList, i);

        switch (i)
        {
        case 0:
            centerX = PyLong_AsLong(pyBoxListItem);
            break;
        case 1:
            centerY = PyLong_AsLong(pyBoxListItem);
            break;
        case 2:
            width = PyLong_AsLong(pyBoxListItem);
            break;
        case 3:
            height = PyLong_AsLong(pyBoxListItem);
            break;
        case 4:
            rotation = PyLong_AsLong(pyBoxListItem);
            break;
        case 5:
            loss = PyFloat_AsDouble(pyBoxListItem);
            break;
        }
    }
    Box box = Box(centerX, centerY, width, height, rotation, loss);

    return box;

}

void BoxContainer::sortByBoxesLossValuesDescending()
{
    _boxes.sort();
    _boxes.reverse();
}

PyObject * BoxContainer::toPyList()
{
    // std::cout << "length of list to convert: " << _boxes.size() << "\n";
    PyObject *pyList = PyList_New( _boxes.size() );
    // TODO: Assign list length more dynamically
    PyObject *pyBox = PyList_New( 6 );

    unsigned int i = 0;
    for (Box & box : _boxes)
    {
        pyBox = BoxContainer::boxToPyList(box);
        PyList_SET_ITEM(pyList, i, pyBox);
        i++;
        // PyList_Append(pyList, pyBox);
    }

    return pyList;
}

PyObject * BoxContainer::boxToPyList(Box box)
{
    // TODO: Assign list length more dynamically
    PyObject *boxAsPyList = PyList_New( 6 );
    PyList_SET_ITEM(boxAsPyList, 0, PyLong_FromLong(box.getCenterX()));
    PyList_SET_ITEM(boxAsPyList, 1, PyLong_FromLong(box.getCenterY()));
    PyList_SET_ITEM(boxAsPyList, 2, PyLong_FromLong(box.getWidth()));
    PyList_SET_ITEM(boxAsPyList, 3, PyLong_FromLong(box.getHeight()));
    PyList_SET_ITEM(boxAsPyList, 4, PyLong_FromLong(box.getRotation()));
    PyList_SET_ITEM(boxAsPyList, 5, PyFloat_FromDouble(box.getLoss()));


    // PyList_Append(boxAsPyList, PyLong_FromLong(box.getCenterX()));
    // PyList_Append(boxAsPyList, PyLong_FromLong(box.getCenterY()));
    // PyList_Append(boxAsPyList, PyLong_FromLong(box.getWidth()));
    // PyList_Append(boxAsPyList, PyLong_FromLong(box.getHeight()));
    // PyList_Append(boxAsPyList, PyLong_FromLong(box.getRotation()));
    // PyList_Append(boxAsPyList, PyFloat_FromDouble(box.getLoss()));

    return boxAsPyList;
}
Box BoxContainer::getBestPick()
{
    return _boxes.front();
}

bool BoxContainer::empty()
{
    return _boxes.size() <= 0;
}

Box BoxContainer::front()
{
    return _boxes.front();
}

void BoxContainer::remove(Box box)
{
    _boxes.remove(box);
}

void BoxContainer::append(Box box)
{
    _boxes.push_back(box);
}

std::list<Box> BoxContainer::getBoxes()
{
    return _boxes;
}