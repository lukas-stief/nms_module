#ifndef _BOX_CONTAINER_H
#define _BOX_CONTAINER_H

#include <iostream>
#include <list>
#include <vector>
// TODO change path
// #include "C:/Python368/include/Python.h"
// #include "C:/Program Files/Python37/include/Python.h"
#include "Python.h"

#include "Box.h"
// #include "RectangleVertices.h"
// #include "C:/Downloads/boost_1_71_0/boost/polygon/polygon.hpp"

class BoxContainer
{
private:
     std::list<Box> _boxes;
public:
    BoxContainer();
    BoxContainer(std::list<Box> boxesListParameter);
    BoxContainer(PyObject *pyList);
    // ~BoxContainer();

    static BoxContainer BoxContainer::fromPyList(PyObject *pyList);
    static Box pyListToBox(PyObject *pyBoxList);

    PyObject * toPyList();
    PyObject * boxToPyList(Box box);

    void sortByBoxesLossValuesDescending();
    Box getBestPick();

    bool empty();
    Box front();
    void append(Box box);
    void remove(Box box);
    std::list<Box> getBoxes();
};

#endif