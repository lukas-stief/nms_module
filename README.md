# physiosense_utils_py
physiosense_utils_py contains utility functions for reading and processing
sensor data.

# Table of Content
1. [Environment Setup](#environment-setup)
    1. [General Setup](#general-setup)
    2. [Project Setup](#project-setup)

# Environment Setup
## General Setup
- [Git](https://git-scm.com/downloads)
    - Go to your preferred target folder and clone the repository. 
    ``` git clone https://bitbucket.org/nms_module.git ```
- [Python 3.6.8](https://www.python.org/downloads/release/python-368/)

- [Visual C++ 2017](https://visualstudio.microsoft.com/vs/older-downloads/)

    ![Installed components](readme_files/image_installed.png)

- [Boost 1.71.0](https://www.boost.org/users/history/version_1_71_0.html) 
([Installation Guide](https://gist.github.com/sim642/29caef3cc8afaa273ce6))




## Project Setup
- Setup the compiler in the c_cpp_properties file:

    ![Add Path to setup.py](readme_files/compiler_setup.png)

- Add the path to the boost and the Python libraries to the include_dirs in the setup.py file.

    ![Add Path to setup.py](readme_files/added_path_to_setup1.png)

- To enable Code completion also add them to the c_cpp_properties includePath:

    ![Add Path to includePath](readme_files/added_path_to_cpp_properties.png)
