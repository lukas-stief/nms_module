#include <deque>

#include "RectangleVertices.h"

// RectangleVertices RectangleVertices::fromBox(Box box)
// {
//     using boost::geometry::append;

//     float angle = (M_PI * box.getRotation()) / 180;
//     float dx = float(box.getWidth()) / float(2);
//     float dy = float(box.getHeight()) / float(2);

//     float dxcos = dx * cos(angle);
//     float dxsin = dx * sin(angle);
//     float dycos = dy * cos(angle);
//     float dysin = dy * sin(angle);

//     point rV1(box.getCenterX() - dxcos + dysin, box.getCenterY() - dxsin - dycos);
//     point rV2(box.getCenterX() + dxcos + dysin, box.getCenterY() + dxsin - dycos);
//     point rV3(box.getCenterX() + dxcos - dysin, box.getCenterY() + dxsin + dycos);
//     point rV4(box.getCenterX() - dxcos - dysin, box.getCenterY() - dxsin + dycos);

//     polygon rectAsPoly;
//     boost::geometry::append(rectAsPoly, rV1);
//     boost::geometry::append(rectAsPoly, rV2);
//     boost::geometry::append(rectAsPoly, rV3);
//     boost::geometry::append(rectAsPoly, rV4);
//     boost::geometry::correct(rectAsPoly);

//     return RectangleVertices(rV1, rV2, rV3, rV4, rectAsPoly);
// }

float RectangleVertices::intersectionAreaWith(RectangleVertices other)
{
    std::deque<polygon> intersectionPoly;

    boost::geometry::intersection(_rectangleAsPolygon, other._rectangleAsPolygon, intersectionPoly);

    double areaOfIntersection = 0;
    for (auto it = intersectionPoly.cbegin(); it != intersectionPoly.cend(); ++it)
    {
        areaOfIntersection += boost::geometry::area(*it);
    }

    return float(areaOfIntersection);
}
