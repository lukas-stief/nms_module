from distutils.core import setup, Extension

module1 = Extension('spam',
                    sources=['spammodule.cpp', 'BoxContainer.cpp', 'Box.cpp', 'NonMaximumSuppression.cpp'],
                    include_dirs=['.', 'C:/Program Files/boost/include/boost-1_71', "C:/Program Files/Python37/include/"])

setup(name = 'spam',
       version='1.0',
       description='This is a demo package',
       ext_modules=[module1])