#ifndef _BOX_H
#define _BOX_H
#define _USE_MATH_DEFINES
#define BOOST_FOREACH

#include <cmath>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <sstream>

// #include "RectangleVertices.h"

class Box
{
    typedef boost::geometry::model::d2::point_xy<double> point;
    typedef boost::geometry::model::polygon<point> polygon;
private:
    const int _centerX;
    const int _centerY;
    const int _width;
    const int _height;
    const int _rotation;
    const double _loss;

    point _rectangleVertex1;
    point _rectangleVertex2;
    point _rectangleVertex3;
    point _rectangleVertex4;
    polygon _rectangleAsPolygon;

    // RectangleVertices rectangleVertices;
public:
    Box(int centerX, int centerY, int width, int height, int rotation, double loss) :
        _centerX(centerX), _centerY(centerY),
        _width(width), _height(height), _rotation(rotation),
        _loss(loss)
    {
        // rectangleVertices = initializeBoxesVertices();
        initializeBoxesVertices();
    }
    // ~Box();
    operator std::string () const {
        std::ostringstream repr;
        repr << "Box: centerX: " << _centerX << " centerY: "
             << _centerY << " width: " << _width << " height: " << _height
             << " rotation: " << _rotation << " loss: " << _loss;
        return repr.str();
    }
    bool operator == (const Box& other) const
    {
        return _centerX == other._centerX && _centerY == other._centerY &&
               _width == other._width && _height == other._height &&
               _rotation == other._rotation && _loss == other._loss;
    }

    bool operator <(const Box &other)
    {
        return _loss < other._loss;
    }

    // RectangleVertices initializeBoxesVertices();
    void initializeBoxesVertices();

    int calculateArea();
    float intersectionAreaWith(polygon other);
    float calculateOverlapWith(Box other);

    int getCenterX();
    int getCenterY();
    int getWidth();
    int getHeight();
    int getRotation();
    double getLoss();
};

#endif