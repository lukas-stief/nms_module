#include "NonMaximumSuppression.h"

NonMaximumSuppression::NonMaximumSuppression(BoxContainer boxes)
{
    _boxContainer = boxes;
}

void NonMaximumSuppression::suppress(float overlapThreshold)
{
    BoxContainer filteredBoxContainer = BoxContainer();

    float overlapArea = 0;
    int bestPickArea = 0;
    while (!_boxContainer.empty())
    {
        Box bestPick = _boxContainer.getBestPick();
        filteredBoxContainer.append(bestPick);

        _boxContainer.remove(bestPick);

        for (Box &box : _boxContainer.getBoxes())
        {

            overlapArea = bestPick.calculateOverlapWith(box) / float(bestPickArea);
            if (overlapArea > overlapThreshold)
            {
                _boxContainer.remove(box);
            }
        }
    }

    _boxContainer = filteredBoxContainer;
}

BoxContainer NonMaximumSuppression::getBoxContainer()
{
    return _boxContainer;
}