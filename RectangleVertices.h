#ifndef _RECTANGLE_VERTICES_H
#define _RECTANGLE_VERTICES_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>

class RectangleVertices
{
    typedef boost::geometry::model::d2::point_xy<double> point;
    typedef boost::geometry::model::polygon<point> polygon;

private:
    // const point _rectangleVertex1;
    // const point _rectangleVertex2;
    // const point _rectangleVertex3;
    // const point _rectangleVertex4;

    // const polygon _rectangleAsPolygon;

    point _rectangleVertex1;
    point _rectangleVertex2;
    point _rectangleVertex3;
    point _rectangleVertex4;
    polygon _rectangleAsPolygon;
public:
    RectangleVertices() {};
    // RectangleVertices(point rV1, point rV2, point rV3, point rV4, polygon rAP) :
    //     _rectangleVertex1(rV1), _rectangleVertex2(rV2), _rectangleVertex3(rV3),
    //     _rectangleVertex4(rV4), _rectangleAsPolygon(rAP) {};

    RectangleVertices(point rV1, point rV2, point rV3, point rV4, polygon rAP)
    {
        _rectangleVertex1 = rV1;
        _rectangleVertex2 = rV2;
        _rectangleVertex3 = rV3;
        _rectangleVertex4 = rV4;
        _rectangleAsPolygon = rAP;
    }
    // static RectangleVertices fromBox(Box box);
    float intersectionAreaWith(RectangleVertices other);

    // polygon getRectangleAsPolygon();
};

#endif