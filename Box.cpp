#include "Box.h"

void Box::initializeBoxesVertices(){
    float angle = (M_PI * _rotation) / 180;
    float dx = float(_width) / float(2);
    float dy = float(_height) / float(2);

    float dxcos = dx * cos(angle);
    float dxsin = dx * sin(angle);
    float dycos = dy * cos(angle);
    float dysin = dy * sin(angle);

    point rV1(_centerX - dxcos + dysin, _centerY - dxsin - dycos);
    point rV2(_centerX + dxcos + dysin, _centerY + dxsin - dycos);
    point rV3(_centerX + dxcos - dysin, _centerY + dxsin + dycos);
    point rV4(_centerX - dxcos - dysin, _centerY - dxsin + dycos);

    polygon rectAsPoly;
    boost::geometry::append(rectAsPoly, rV1);
    boost::geometry::append(rectAsPoly, rV2);
    boost::geometry::append(rectAsPoly, rV3);
    boost::geometry::append(rectAsPoly, rV4);
    boost::geometry::correct(rectAsPoly);

    _rectangleVertex1 = rV1;
    _rectangleVertex2 = rV2;
    _rectangleVertex3 = rV3;
    _rectangleVertex4 = rV4;
    _rectangleAsPolygon = rectAsPoly;
    // return RectangleVertices(rV1, rV2, rV3, rV4, rectAsPoly);
}


int Box::calculateArea()
{
    return _width * _height;
}

float Box::calculateOverlapWith(Box other)
{
    return intersectionAreaWith(other._rectangleAsPolygon);
}

float Box::intersectionAreaWith(polygon other)
{
    std::deque<polygon> intersectionPoly;

    boost::geometry::intersection(_rectangleAsPolygon, other, intersectionPoly);

    double areaOfIntersection = 0;
    for (auto it = intersectionPoly.cbegin(); it != intersectionPoly.cend(); ++it)
    {
        areaOfIntersection += boost::geometry::area(*it);
    }

    return float(areaOfIntersection);
}

int Box::getCenterX()
{
    return _centerX;
}

int Box::getCenterY()
{
    return _centerY;
}

int Box::getHeight()
{
    return _height;
}

int Box::getWidth()
{
    return _width;
}

int Box::getRotation()
{
    return _rotation;
}

double Box::getLoss()
{
    return _loss;
}